import {useContext} from "react";

import {NavLink} from "react-router-dom";

// shorthand method
import {Container, Nav, Navbar} from "react-bootstrap"

import UserContext from "../UserContext";

import Image from "../images/ibywind_logo.png";

import "../App.css"

export default function AppNavbar(){

    const {user} = useContext(UserContext);

    console.log(user);

	return(
	<Navbar className="font-link" bg="dark" variant="dark" expand="lg" sticky="top">
      <Container fluid>
        <img 
            src={Image} 
            width="30"
            height="30"
            className="me-3"
            alt="ibywind ph logo"
          />
        <Navbar.Brand as={NavLink} to="/">Ibywind Philippines</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">

          <Nav className="ms-auto">
            <Nav.Link as={NavLink} to="/" end>Home</Nav.Link>
            {
              (user.isAdmin)
              ?
              <Nav.Link as={NavLink} to="/admin" end>Admin</Nav.Link>
              :
              <>
              <Nav.Link as={NavLink} to="/products" end>Products</Nav.Link>
              </>
            }

            {
              (user.isAdmin === false)
              ?
              <Nav.Link as={NavLink} to="/orders" end>Orders</Nav.Link>
              :
              <></>
            }
            
            {
              (user.id !== null)
              ?
                <Nav.Link as={NavLink} to="/logout" end>Logout</Nav.Link>
              :
              <>
                <Nav.Link as={NavLink} to="/login" end>Login</Nav.Link>
                <Nav.Link as={NavLink} to="/register" end>Register</Nav.Link>
              </>
            }
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>

	)
}
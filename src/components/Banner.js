import {useContext} from "react";

import {Row, Col, Button } from "react-bootstrap";

import { Link } from "react-router-dom";

import Image from "../images/banner.png";
import Image2 from "../images/easyinstaller.png";
import Image3 from "../images/newprod_.png";

import Carousel from 'react-bootstrap/Carousel';

import UserContext from "../UserContext";

export default function Banner({bannerProp}){

const { title, description, destination, label } = bannerProp;

const {user} = useContext(UserContext);

    return (
        <>
      <Carousel className="pt-5">
      <Carousel.Item>
        <img
          className="d-block w-100"
          src={Image}
          alt="First slide"
        />
      </Carousel.Item>

      <Carousel.Item>
        <img
          className="d-block w-100"
          src={Image2}
          alt="Second slide"
        />
      </Carousel.Item>

      <Carousel.Item>
        <img
          className="d-block w-100"
          src={Image3}
          alt="Third slide"
        />
      </Carousel.Item>

    </Carousel>
    
        <Row>
            <Col className="py-4 mb-4 text-center font-link">
                <h1>{title}</h1>
                <p className="banner-paragraph mb-3">{description}</p>
                {
                  (user.isAdmin)
                  ?
                  <Button as={Link} to="/admin" variant="dark">Admin Dashboard</Button>
                  :
                  <Button as={Link} to={destination} variant="dark">{label}</Button>
                }
            </Col>
        </Row>
        </>
    )
}


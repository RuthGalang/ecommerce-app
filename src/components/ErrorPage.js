import {useContext} from "react";

import { Container, Row, Col, Button } from "react-bootstrap";

import { Link } from "react-router-dom";

import Image from "../images/404.png";

import UserContext from "../UserContext";

export default function ErrorPage(){

const {user} = useContext(UserContext);

	return (
		<>
		<Container className="pt-5 text-center"> 
		<img
          className="w-50"
          src={Image}
          alt="error image"
        />
		<Row>
            <Col className="py-4 mb-4 text-center font-link">
                <p className="banner-paragraph mb-3">The page you are looking for can not be found</p>
                {
                  (user.isAdmin)
                  ?
                  <Button as={Link} to="/admin" variant="dark">Admin Dashboard</Button>
                  :
                  <Button as={Link} to="/" variant="dark">Back to home</Button>
                }
            </Col>
        </Row>
        </Container>
	</>
	)
}
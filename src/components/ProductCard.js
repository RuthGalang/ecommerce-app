import {Row, Col, Container, Card, Button} from "react-bootstrap";
import {Link} from "react-router-dom";

import "../App.css";

export default function ProductCard({productProp}) {
 
    const { _id, productName, description, price, stocks } = productProp;
    console.log(productProp);

	return (
      <Container fluid className="text-center">
        <Row className="mb-3 h-100">
         <Col className="my-3 p-3">
    		   <Card className="prod-card mt-3 mx-2 h-100">
                <Card.Body>
                 <Card.Title className="fs-4 fw-bold mb-3">
                    {productName}
                 </Card.Title>
                 <Card.Subtitle className="fs-5">
                    Description:
                 </Card.Subtitle>
                 <Card.Text className="fs-5">
                    {description}
                 </Card.Text>
                             
                 <Card.Subtitle className="fs-5">
                    Price:
                 </Card.Subtitle>
                 <Card.Text className="fs-5">
                    ₱ {price}
                 </Card.Text>

                 <Card.Subtitle className="fs-5">
                    Stocks:
                 </Card.Subtitle>
                 <Card.Text className="fs-5">
                    {stocks} available
                 </Card.Text>

                 <Button as={Link} to={`/products/${_id}`} variant="dark">Details</Button>
                </Card.Body>
            </Card>
          </Col>  
         </Row> 
      </Container>       
	)
}
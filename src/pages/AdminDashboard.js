import {useContext, useEffect, useState} from "react";

import {Button, Table,  Modal, Form} from "react-bootstrap";

import {Navigate} from "react-router-dom";
import Swal from "sweetalert2";

import UserContext from "../UserContext";

export default function AdminDashboard(){

	const {user} = useContext(UserContext);

	// Create an allCourses state to contain all the courses from the response of our fetch data.
	const [allProducts, setAllProducts] = useState([]);

	// Create an allUsers state to contain all the courses from the response of our fetch data.
	const [allUsers, setAllUsers] = useState([]);

	// State hooks to store the values of the input fields for our modal.
	const [productId, setProductId] = useState("");
	const [productName, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
   const [stocks, setStocks] = useState(0);

    // State to determine whether submit button in the modal is enabled or not
    const [isActive, setIsActive] = useState(false);

    // State for Add/Edit/ShowUser Modal
   const [showAdd, setShowAdd] = useState(false);
	const [showEdit, setShowEdit] = useState(false);
	const [showUsers, setShowUsers] = useState(false);


	// To control the add course modal pop out
		const openAdd = () => setShowAdd(true); //Will show the modal
		const closeAdd = () => setShowAdd(false); //Will hide the modal

	// To control the show users modal pop out
		const openShowUsers = () => setShowUsers(true);
		const closeShowUsers = () => setShowUsers(false);


	// To control the edit course modal pop out
	// We have passed a parameter from the edit button so we can retrieve a specific course and bind it with our input fields.
		const openEdit = (id) => {
			setProductId(id);

			// Getting a specific course to pass on the edit modal
			fetch(`${ process.env.REACT_APP_API_URL }/products/${id}`)
			.then(res => res.json())
			.then(data => {

				console.log(data);

				// updating the course states for editing
				setName(data.productName);
				setDescription(data.description);
				setPrice(data.price);
				setStocks(data.stocks);
			});

			setShowEdit(true)
		};

		const closeEdit = () => {

			// Clear input fields upon closing the modal
		    setName('');
		    setDescription('');
		    setPrice(0);
		    setStocks(0);

			setShowEdit(false);
		};


	const fetchUserData = () => {
		fetch(`${process.env.REACT_APP_API_URL}/users/getAll`, {
			headers: {
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setAllUsers(data.map(user => {
				return(
					<tr key={user._id}>
						<td>{user._id}</td>
						<td>{user.firstName}</td>
						<td>{user.lastName}</td>
						<td>{user.email}</td>
						<td>{user.isAdmin ? "Admin" : "Not Admin"}</td>
						<td>
							{
								//Conditional rendering on what button should be visible base on the status of the user
								(user.isAdmin)
								?
								<Button variant="danger" size="sm" onClick={()=> notAdminStatus(user._id, user.firstName)}>Set as User</Button>
								:
								<>
									<Button variant="success" size="sm" onClick={()=> adminStatus(user._id, user.firstName)}>Set as Admin</Button>
								</>
							}
						</td>
					</tr>
				)
			}));
		});
	}

	useEffect(() => {
		fetchUserData();
	}, [])	

	const notAdminStatus = (id, firstName) => {
		console.log(id);
		console.log(firstName);

		fetch(`${process.env.REACT_APP_API_URL}/users/${id}/setAsAdmin`, {
			method: "PATCH",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isAdmin: false
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data) {
				Swal.fire ({
					title: "Setting admin status went successful",
					icon: "success",
					text: `${firstName} is now an admin.`
				});

				fetchUserData();
			}
			else {
				Swal.fire ({
					title: "Setting admin status went unsuccessful",
					icon: "error",
					text: `Something went wrong, please try again later.`
				});
			}
		})
	}

	const adminStatus = (id, firstName) => {
		console.log(id);
		console.log(firstName);

		fetch(`${process.env.REACT_APP_API_URL}/users/${id}/setAsAdmin`, {
			method: "PATCH",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isAdmin: true
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data) {
				Swal.fire ({
					title: "Setting admin status went successful",
					icon: "success",
					text: `${firstName} is not an admin.`
				});

				fetchUserData();
			}
			else {
				Swal.fire ({
					title: "Setting admin status went unsuccessful",
					icon: "error",
					text: `Something went wrong, please try again later.`
				});
			}
		})
	}


	// fetchData() function to get all the active/inactive courses.
	const fetchData = () => {
		// get all the courses from the database
		fetch(`${process.env.REACT_APP_API_URL}/products/all`, {
			headers: {
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setAllProducts(data.map(product => {
				return(
					<tr key={product._id}>
						<td>{product._id}</td>
						<td>{product.productName}</td>
						<td>{product.description}</td>
						<td>{product.price}</td>
						<td>{product.stocks}</td>
						<td>{product.isActive ? "Active" : "Inactive"}</td>
						<td>
							{
								(product.isActive)
								?
								<Button variant="danger" size="sm" onClick={()=> archive(product._id, product.productName)}>Archive</Button>
								:
								<>
									<Button variant="success" size="sm" className="mx-1" onClick={()=> unarchive(product._id, product.productName)}>Unarchive</Button>
									<Button variant="secondary" size="sm" className="mx-1" onClick={() => openEdit(product._id)}>Edit</Button>
								</>
							}
						</td>
					</tr>
				)
			}));
		});
	}

		useEffect(() => {
			fetchData();
		}, [])

	const archive = (id, productName) => {
		console.log(id);
		console.log(productName);


		fetch(`${process.env.REACT_APP_API_URL}/products/archive/${id}`, {
			method: "PATCH",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isActive: false
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data) {
				Swal.fire ({
					title: "Archive successful",
					icon: "success",
					text: `${productName} is now inactive.`
				});

				fetchData();
			}
			else {
				Swal.fire ({
					title: "Archive unsuccessful",
					icon: "error",
					text: `Something went wrong, please try again later.`
				});
			}
		})
	}

	const unarchive = (id, productName) => {
		console.log(id);
		console.log(productName);

		fetch(`${process.env.REACT_APP_API_URL}/products/archive/${id}`, {
			method: "PATCH",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isActive: true
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data) {
				Swal.fire ({
					title: "Unarchive successful",
					icon: "success",
					text: `${productName} is now active.`
				});

				fetchData();
			}
			else {
				Swal.fire ({
					title: "Unarchive unsuccessful",
					icon: "error",
					text: `Something went wrong, please try again later.`
				});
			}
		})
	}


		const addProduct = (e) =>{
				
			    e.preventDefault();

			    fetch(`${process.env.REACT_APP_API_URL}/products/addProduct`, {
			    	method: "POST",
			    	headers: {
						"Content-Type": "application/json",
						"Authorization": `Bearer ${localStorage.getItem('token')}`
					},
					body: JSON.stringify({
					    productName: productName,
					    description: description,
					    price: price,
					    stocks: stocks
					})
			    })
			    .then(res => res.json())
			    .then(data => {
			    	console.log(data);

			    	if(data){
			    		Swal.fire({
			    		    title: "Product succesfully Added",
			    		    icon: "success",
			    		    text: `${productName} is now added`
			    		});

			    		fetchData();
			    		
			    		closeAdd();
			    	}
			    	else{
			    		Swal.fire({
			    		    title: "Error!",
			    		    icon: "error",
			    		    text: `Something went wrong. Please try again later!`
			    		});
			    		closeAdd();
			    	}

			    })

			    setName('');
			    setDescription('');
			    setPrice(0);
			    setStocks(0);
		}

		
		const editProduct = (e) =>{
				
			    e.preventDefault();

			    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
			    	method: "PUT",
			    	headers: {
						"Content-Type": "application/json",
						"Authorization": `Bearer ${localStorage.getItem('token')}`
					},
					body: JSON.stringify({
					    productName: productName,
					    description: description,
					    price: price,
					    stocks: stocks
					})
			    })
			    .then(res => res.json())
			    .then(data => {
			    	console.log(data);

			    	if(data){
			    		Swal.fire({
			    		    title: "Product succesfully Updated",
			    		    icon: "success",
			    		    text: `${productName} is now updated`
			    		});

			    		fetchData();
			    		
			    		closeEdit();

			    	}
			    	else{
			    		Swal.fire({
			    		    title: "Error!",
			    		    icon: "error",
			    		    text: `Something went wrong. Please try again later!`
			    		});

			    		closeEdit();
			    	}

			    })

			    setName('');
			    setDescription('');
			    setPrice(0);
			    setStocks(0);
		} 

		useEffect(() => {

	        if(productName !== "" && description !== "" && price > 0 && stocks > 0){
	            setIsActive(true);
	        } else {
	            setIsActive(false);
	        }

	    }, [productName, description, price, stocks]);



	return (
		(user.isAdmin)
		?
		<>
			<div className="mt-5 mb-3 text-center">
				<h1>Admin Dashboard</h1>
				
				<Button variant="primary" className="mx-2 my-4" onClick={openAdd}>Add Product</Button>
				
				<Button variant="secondary" className="mx-2 my-4" onClick={openShowUsers}>View All Users</Button>
			</div>

			<Table striped bordered hover>
			     <thead>
			       <tr>
			         <th>Product ID</th>
			         <th>Product Name</th>
			         <th>Description</th>
			         <th>Price</th>
			         <th>Stocks</th>
			         <th>Status</th>
			         <th>Actions</th>
			       </tr>
			     </thead>
			     <tbody>
			       {allProducts}
			     </tbody>
			   </Table>

			{/*Modal for showing all users*/}
			<Modal 
	        	show={showUsers} 
	        	size={"xl"}
	        	backdrop="static" 
	        	keyboard={false} 
	        	onHide={closeShowUsers} 
	        	aria-labelledby="contained-modal-title-vcenter" centered>

	        <Modal.Header closeButton>
	    				<Modal.Title>All Users Details</Modal.Title>
	    			</Modal.Header>

	    		<Modal.Body>
	    			<Table striped bordered hover>
			     <thead>
			       <tr>
			         <th>User ID</th>
			         <th>First Name</th>
			         <th>Last Name</th>
			         <th>Email</th>
			         <th>Status</th>
			         <th>Action</th>
			       </tr>
			     </thead>
			     <tbody>
			       {allUsers}
			     </tbody>
			   </Table>

			  </Modal.Body>
			</Modal>

			
			{/*Modal for Adding a new product*/}
	        <Modal 
	        	show={showAdd} 
	        	backdrop="static" 
	        	keyboard={false} 
	        	onHide={closeAdd} 
	        	aria-labelledby="contained-modal-title-vcenter" centered>

	    		<Form onSubmit={e => addProduct(e)}>

	    			<Modal.Header closeButton>
	    				<Modal.Title>Add New Product</Modal.Title>
	    			</Modal.Header>

	    			<Modal.Body>
	    	        	<Form.Group controlId="name" className="mb-3">
	    	                <Form.Label>Product Name</Form.Label>
	    	                <Form.Control 
	    		                type="text" 
	    		                placeholder="Enter Product Name" 
	    		                value = {productName}
	    		                onChange={e => setName(e.target.value)}
	    		                required
	    	                />
	    	            </Form.Group>

	    	            <Form.Group controlId="description" className="mb-3">
	    	                <Form.Label>Product Description</Form.Label>
	    	                <Form.Control
	    	                	as="textarea"
	    	                	rows={3}
	    		                placeholder="Enter Product Description" 
	    		                value = {description}
	    		                onChange={e => setDescription(e.target.value)}
	    		                required
	    	                />
	    	            </Form.Group>

	    	            <Form.Group controlId="price" className="mb-3">
	    	                <Form.Label>Price</Form.Label>
	    	                <Form.Control 
	    		                type="number" 
	    		                placeholder="Enter Price" 
	    		                value = {price}
	    		                onChange={e => setPrice(e.target.value)}
	    		                required
	    	                />
	    	            </Form.Group>

	    	            <Form.Group controlId="stocks" className="mb-3">
	    	                <Form.Label>Stocks</Form.Label>
	    	                <Form.Control 
	    		                type="number" 
	    		                placeholder="Enter Stocks" 
	    		                value = {stocks}
	    		                onChange={e => setStocks(e.target.value)}
	    		                required
	    	                />
	    	            </Form.Group>
	    			</Modal.Body>

	    			<Modal.Footer>
	    				{ isActive 
	    					? 
	    					<Button variant="success" type="submit" id="submitBtn">
	    						Save
	    					</Button>
	    				    : 
	    				    <Button variant="danger" type="submit" id="submitBtn" disabled>
	    				    	Save
	    				    </Button>
	    				}
	    				<Button variant="secondary" onClick={closeAdd}>
	    					Close
	    				</Button>
	    			</Modal.Footer>

	    		</Form>	
	    	</Modal>
	    {/*End of modal for adding product*/}

    	{/*Modal for Editing a product*/}
        <Modal 
        	show={showEdit} 
        	backdrop="static" 
        	keyboard={false} 
        	onHide={closeEdit}
        	aria-labelledby="contained-modal-title-vcenter" centered>

    		<Form onSubmit={e => editProduct(e)}>

    			<Modal.Header closeButton>
    				<Modal.Title>Edit a Product</Modal.Title>
    			</Modal.Header>

    			<Modal.Body>
    	        	<Form.Group controlId="name" className="mb-3">
    	                <Form.Label>Product Name</Form.Label>
    	                <Form.Control 
    		                type="text" 
    		                placeholder="Enter Product Name" 
    		                value = {productName}
    		                onChange={e => setName(e.target.value)}
    		                required
    	                />
    	            </Form.Group>

    	            <Form.Group controlId="description" className="mb-3">
    	                <Form.Label>Course Description</Form.Label>
    	                <Form.Control
    	                	as="textarea"
    	                	rows={3}
    		                placeholder="Enter Product Description" 
    		                value = {description}
    		                onChange={e => setDescription(e.target.value)}
    		                required
    	                />
    	            </Form.Group>

    	            <Form.Group controlId="price" className="mb-3">
    	                <Form.Label>Price</Form.Label>
    	                <Form.Control 
    		                type="number" 
    		                placeholder="Enter Price" 
    		                value = {price}
    		                onChange={e => setPrice(e.target.value)}
    		                required
    	                />
    	            </Form.Group>

    	            <Form.Group controlId="stocks" className="mb-3">
    	                <Form.Label>Stocks</Form.Label>
    	                <Form.Control 
    		                type="number" 
    		                placeholder="Enter Course Slots" 
    		                value = {stocks}
    		                onChange={e => setStocks(e.target.value)}
    		                required
    	                />
    	            </Form.Group>
    			</Modal.Body>

    			<Modal.Footer>
    				{ isActive 
    					? 
    					<Button variant="success" type="submit" id="submitBtn">
    						Save
    					</Button>
    				    : 
    				    <Button variant="danger" type="submit" id="submitBtn" disabled>
    				    	Save
    				    </Button>
    				}
    				<Button variant="secondary" onClick={closeEdit}>
    					Close
    				</Button>
    			</Modal.Footer>

    		</Form>	
    	</Modal>
    	{/*End of modal for editing a product*/}


		</>
		:
		<Navigate to="/products" />
	)
}
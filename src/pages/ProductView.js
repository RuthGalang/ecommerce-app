import {useState, useEffect, useContext} from "react";

import { Container, Card, Button, Row, Col, Form} from "react-bootstrap";
import {useParams, useNavigate, Link} from "react-router-dom";
import Swal from "sweetalert2";

import UserContext from "../UserContext";

export default function ProductView(){

	const {user} = useContext(UserContext);
	// "useParams" hook allows us to retrieve the courseId passed via the URL.
	const {productId} = useParams();
	const navigate = useNavigate();

	// Create state hooks to capture the information of a specific product and display it in our application.

	const [productName, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);
	const [quantity, setQuantity] = useState('');
	const [stocks, setStocks] = useState(0);

	const [isActive, setIsActive] = useState(false);

	// For button
	useEffect(()=>{

		// Enable the button if:
			// Quantity fields are populated
		if(quantity <= 0) {
			setIsActive(false);
		} else {
			setIsActive(true);
		}
	}, [quantity])


	useEffect(() => {
		console.log(productId);

		fetch(`${process.env.REACT_APP_API_URL }/products/${productId}`)
				.then(res => res.json())
				.then(data => {

				    console.log(data);

				    setName(data.productName);
				    setDescription(data.description);
				    setPrice(data.price);
				    setStocks(data.stocks);

		});	
	}, [productId]);
							
	const order = (productId) => {
		fetch(`${ process.env.REACT_APP_API_URL }/users/createOrder`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				products: [{
					productId: productId,
					productName: productName,
					quantity: quantity
					}],
				totalAmount: quantity*price
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);
			if(data) {
				if(quantity === 0 || quantity <= 0){
					Swal.fire ({
					title: "Error",
					icon: "error",
					text: "Please input a quantity."
					});
				}

				else {
					Swal.fire ({
					title: "Thank you for purchasing!",
					icon: "success",
					text: "Your order have been placed, happy shopping!"
					});
					navigate("/products");
				}
			}
			else {
				Swal.fire ({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				});
			}
		});
	}

    return(
        <Container className="mt-5">
            <Row>
                <Col lg={{ span: 6, offset: 3 }}>
                    <Card>
                        <Card.Body className="text-center">
                            <Card.Title className="fs-4 fw-bold mb-3">{productName}</Card.Title>
                            <Card.Subtitle className="fs-5">Description:</Card.Subtitle>
                            <Card.Text className="fs-5 mb-3">{description}</Card.Text>
                            <Card.Subtitle className="fs-5">Price:</Card.Subtitle>
                            <Card.Text className="fs-5 mb-3">₱ {price}</Card.Text>
                            <Card.Subtitle className="fs-5">Stocks:</Card.Subtitle>
                            <Card.Text className="fs-5 mb-3">{stocks} available</Card.Text>
                            <Form className="px-5 mb-3">
                            <Form.Label className="fs-5">Quantity:</Form.Label>
                            <Form.Control 
                            	type="number" 
                            	placeholder="Input quantity" 
                            	value={quantity}
                            	min={0}
                            	onChange={e => setQuantity(e.target.value)}
                            	/>
                            </Form>
                            <Container>
                            {
                            	(user.id !== null)
                            	?
                            		isActive
                            		?
                            		<Button variant="primary" size="md" onClick={() => order(productId)}>Order</Button>
                            		:
                            		<Button variant="primary" size="md" onClick={() => order(productId)} disabled>Order</Button>
                            	:
                            	<Button as={Link} to="/login" variant="success" size="md">Login to Order</Button>
                            }
                            </Container>

                        </Card.Body>        
                    </Card>
                </Col>
            </Row>
        </Container>
        
    )
}